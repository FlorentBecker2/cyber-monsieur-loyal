#! /usr/bin/env python3


#### IMPORT ############
from flask import Flask
import os.path
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_breadcrumbs import Breadcrumbs



def mkpath(p):
    return os.path.normpath(os.path.join(os.path.dirname(__file__), p))



#### PARAMS ############
app = Flask(__name__)
app.debug = True
Breadcrumbs(app = app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["SQLALCHEMY_DATABASE_URI"] = ("sqlite:///" + mkpath("../myapp.db"))
app.config["SECRET_KEY"] = "d9ee54d5-f044-49da-9489-c976a146cfaa"
db = SQLAlchemy(app)

login_manager= LoginManager(app)

# Configuration des répetoires d'image
app.config["DIRECTORY_EQUIPE_IMAGE"]    = "img/upload/equipe/"
app.config["DIRECTORY_CONCOURS_IMAGE"]  = "img/upload/concours/"
app.config["DIRECTORY_CONCOURS_SUJET"]  = "sujet/"
app.config["DIRECTORY_THEME_IMAGE"]     = "img/upload/theme/"
app.config["DIRECTORY_UTILES_IMAGE"]    = "img/logos_utiles/"
app.config["ALLOWED_EXTENSIONS"]        = {'jpg', 'jpe', 'jpeg', 'png', 'gif', 'svg', 'bmp'}

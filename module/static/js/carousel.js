/**
 * Déplacer les cards dans le carousel en fontion
 * du bouton sur lequel un "click" est éffectué.
 */
document.addEventListener("DOMContentLoaded", function(){
    let bouton_gauche = document.querySelectorAll(".conteneur-bouton")[0].children[0];
    let bouton_droite = document.querySelectorAll(".conteneur-bouton")[1].children[0];
    let liste_concours = document.querySelector(".liste-concours");
    let incrementation = 300;

    if(liste_concours.childElementCount < 4){
        for(elem of liste_concours.children){
            elem.style.margin = "auto";
        }
        bouton_droite.classList.add("disabled");
        bouton_gauche.classList.add("disabled");
    }
    
    // Aller à droite
    bouton_droite.addEventListener("click", function(){
        let chaine = liste_concours.style.transform;
        let valeur_precedente = parseInt(chaine.substring(11, chaine.length - 3));
        let valeur = valeur_precedente - incrementation;
        if(valeur/incrementation > -(liste_concours.childElementCount-2)){
            liste_concours.style.transform = "translateX(" + valeur + "px)";
        }
        if((valeur-incrementation)/incrementation > -(liste_concours.childElementCount-2)){
            bouton_gauche.classList.remove("disabled");
        }
        else {
            bouton_gauche.classList.remove("disabled");
            bouton_droite.classList.add("disabled");
        }
    });

    // Aller à gauche
    bouton_gauche.addEventListener("click", function(){
        let chaine = liste_concours.style.transform;
        let valeur_precedente = parseInt(chaine.substring(11, chaine.length - 3));
        let valeur = valeur_precedente + incrementation;
        if(valeur_precedente <= -incrementation){
            liste_concours.style.transform = "translateX(" + valeur + "px)";
        }
        if(valeur_precedente+incrementation <= -incrementation){
            bouton_droite.classList.remove("disabled");
        }
        else {
            bouton_droite.classList.remove("disabled");
            bouton_gauche.classList.add("disabled");
        }
    });
});
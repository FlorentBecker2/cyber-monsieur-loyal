# Tuto

- lancer sqlite : `sqlite3 [fichier]`
- lire un fichier : `.read [fichier]`
- créer un fichier bd : `.save [nom].db`
- Lire toutes les tables: `.tables`
- Quitter : `.exit`


# Compte utilisateur
**Organisateurs :**

    login: organisateur
    mdp: organisateur

**Administrateur :**

    login : admin
    mdp : 123456789

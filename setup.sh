#! /bin/bash

# Création du virtualenv et activation
virtualenv ../venv -p python3
source ../venv/bin/activate

# Chargement des plugins
pip install -r requirement.txt

# Création et chargement de la base de données
flask crea-db
flask load-db